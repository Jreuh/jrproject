## BASE DE PROJET BEDROCK

Cette **BASE** est une base WP Bedrock.

Bedrock est déjà installé, afin de l'utiliser pour init un projet il faut suivre les étapes suivantes :


0. On crée un projet gitlab vide au nom du projet ;)
1. Cloner la base en lui indiquant le nom du projet (afin de créer notre dossier)
```bash
git clone git@gitlab.weareled.fr:devled/base-wp-bedrock-project.git <nom-de-mon-projet>
```
2. On rentre dans le dossier afin de lui attribuer le bon remote projet et on va créer la branche staging
```bash
cd nom-de-mon-projet
sudo rm -R .git (on supprime le git du projet base)
git init
git remote add origin <url-ssh-de-mon-repo-gitlab>
git add .
git commit -m "Initial commit"
git push -u origin master
git checkout -b staging
```
3. Rechercher dans .project-basics les occurences NOMDEMONPROJET et les remplacer par le vrai nom du projet :)
4. Copier/coller Makefile.dist (se trouve dans .project-basics) à la racine du projet et le renommer en Makefile
5. Créer le .env, copier/coller le contenu de .env.example dans notre .env en mettant les bonnes informations :
```bash
Décommenter la ligne DATABASE_URL et y mettre les information se trouvant dans .project-basics/envs/mysql.env
Décommenter la ligne DB_PREFIX, mettre comme préfix de bdd "wp_nom-du-projet_"
Modifier le WP_HOME avec notre url en .docker (on utlise le nom du projet)
Générer les keys via le site https://roots.io/salts.html puis remplacer les lignes de .env
```
6. Lancer les containers
```bash
make up
```
7. On installe les différentes dépendences via composer via le container php :
```bash
make ex
composer install
```
8. Créer le thème de notre projet (on utilise le nom du projet) via le container php :
```bash
make ex (si on est sortie du container on y retounre ;) )
cd /web/app/themes
composer create-project roots/sage nom-de-notre-projet dev-main
```
9. On install yarn dans notre thème puis on build une première fois nos ressources via le container php :
```bash
make ex (si on est sortie du container on y retounre ;) )
cd /web/app/themes/nom-de-notre-projet
yarn
yarn build
```
10. On modifie le fichier bud.config.js qui se trouve dans notre thème afin de mettre la bonne url sur les lignes proxy et serve, exemple :
```bash
/**
* Target URL to be proxied by the dev server.
*
* This is your local dev server.
*/
.proxy('http://lafabrique.docker')

/**
* Development URL
*/
.serve('http://lafabrique.docker');
```
11. On fait un beau README afin d'expliquer à tout le monde comment installer le projet en local et comment travailler dessus.
