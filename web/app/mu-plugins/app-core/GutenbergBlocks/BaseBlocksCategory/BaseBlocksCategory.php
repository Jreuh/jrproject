<?php


namespace Weareled\GutenbergBlocks\BaseBlocksCategory;


class BaseBlocksCategory
{
    public function __construct()
    {
        add_filter('block_categories', [$this, 'base_block_category'], 10, 2);
    }

    public function base_block_category($categories)
    {
        $base_block_category =
            [
                'slug' => 'base-blocks',
                'title' => __('Base Blocks', 'base-blocks'),
            ];

        array_unshift($categories, $base_block_category);
        return $categories;
    }
}