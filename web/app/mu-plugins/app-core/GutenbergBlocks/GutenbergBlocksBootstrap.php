<?php

namespace Weareled\GutenbergBlocks;

use Weareled\GutenbergBlocks\ExempleBlock\ExempleBlock;
use Weareled\GutenbergBlocks\BaseBlocksCategory\BaseBlocksCategory;


class GutenbergBlocksBootstrap
{
    public function __construct()
    {
        // Custom Gutenberg Collection
        new BaseBlocksCategory();

        // Custom Gutenberg Blocks
        new ExempleBlock();
    }
}