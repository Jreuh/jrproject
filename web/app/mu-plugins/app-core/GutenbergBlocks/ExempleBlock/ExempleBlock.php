<?php

declare(strict_types=1);

namespace Weareled\GutenbergBlocks\ExempleBlock;

use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Fields\Textarea;
use WordPlate\Acf\Location;
use WordPlate\Acf\Fields\WysiwygEditor;

class ExempleBlock
{
    public function __construct()
    {
        add_action('init', [$this, 'registerBlock']);
    }

    public function registerBlock()
    {
        acf_register_block_type([
            'name' => 'exemple-block',
            'title' => 'Exemple',
            'description' => 'Un bloc Gutenberg exemple',
            'render_template' => plugin_dir_path( __FILE__ ) . '/template-parts/template.php',
            'category' => 'mfr-blocks',
            'icon' => 'editor-alignleft',
            'keywords' => ['exemple, mfr'],
            'enqueue_assets' => function() {
                wp_enqueue_style('exemple-block', plugin_dir_url( __FILE__ ) . '/assets/css/style.css', '', '1.0.0', 'all');
                wp_enqueue_script('exemple-block', plugin_dir_url( __FILE__ ) . '/assets/js/main.js', '', '1.0.0', 'all');
            }
        ]);

        register_extended_field_group([
            'title' => 'exemple',
            'fields' => [
                Text::make('Titre', 'title_exemple_block'),
                Textarea::make('Texte', 'text_exemple_block'),
                WysiwygEditor::make('Editeur de texte')
            ],
            'location' => [
                Location::if('block', 'acf/exemple-block')
            ],
        ]);
    }
}
