<?php
/**
 * Plugin Name: Weareled App Core
 * Plugin URI: https://Weareled.fr
 * Description: Core mu plugins
 * Version: 1.0
 * Author: Weareled
 *
 * @package app-core
 */

namespace Weareled;

define('DEV_AGENCIES', ['Weareled']); // alias des agences qui développent sous WordPress

App::init();
