<?php
/**
 * Plugin Name: App-Core
 * Plugin URI: https://Weareled.fr
 * Description: Core Website mu plugins
 * Version: 1.0
 * Author: Weareled
 *
 * @package app-core
 */

namespace Weareled;

use Weareled\Fields\FieldsBootstrap;
use Weareled\GutenbergBlocks\GutenbergBlocksBootstrap;
use Weareled\OptionPage\OptionPageBootstrap;

/**
 * Class App : Core loader
 * @package Weareled
 */
class App
{

    /**
     * Initialize class.
     *
     * @return void
     * @since 3.3.0
     *
     */
    public static function init()
    {
        new OptionPageBootstrap();
        new GutenbergBlocksBootstrap();
        new FieldsBootstrap();

        self::manage_environment();
    }

    /**
     * Active Query Monitor uniquement en développement
     *
     * @return void
     * @since 3.3.0
     *
     */
    public static function manage_environment()
    {
        if (defined('WP_ENV') && WP_ENV !== 'development') {
            add_filter('acf/settings/show_admin', '__return_false');
            define('QM_DISABLED', true);
        }
    }
}
