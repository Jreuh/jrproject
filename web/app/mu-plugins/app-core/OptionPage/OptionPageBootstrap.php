<?php

namespace Weareled\OptionPage;

use Weareled\OptionPage\BaseOptionPage\BaseOptionPage;

class OptionPageBootstrap
{
    public function __construct()
    {
        new BaseOptionPage();
    }
}
