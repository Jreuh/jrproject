<?php


namespace Weareled\OptionPage\BaseOptionPage;

class BaseOptionPage
{
    public function __construct()
    {
        add_action('init', [$this, 'registerOptionPage']);
    }

    public function registerOptionPage()
    {
        acf_add_options_page([
            'icon_url' => 'dashicons-admin-appearance',
            'menu_slug' => 'base',
            'page_title' => 'BASE',
            'post_id'   => 'base',
            'position' => 2,
        ]);
    }
}